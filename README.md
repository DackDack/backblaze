## Backblaze B2 for PHP

`backblaze-b2` is the SDK for working with Backblaze's B2 storage service (compatible with API v2).

## Install

Via Composer

``` bash
$ composer misterspelik/backblaze-b2
```

## Usage

``` php
use BackblazeB2\Client;
use Backblaze\Storage\Bucket;
use Backblaze\Storage\File;
use Illuminate\Support\Optional;

$options = [
    'auth_timeout_seconds' => seconds,
    'download_valid_duration_seconds'=>seconds
];

$client = new Client('applicationKeyId', 'applicationKey', $options);
```
_$options_ is optional. If omitted, the default timeout is 12 hours. The timeout allows for a long lived Client object so that the authorization token does not expire.

#### Returns a bucket details
``` php
$bucket = $client->createBucket([
    'BucketName' => 'my-special-bucket',
    'BucketType' => Bucket::TYPE_PRIVATE // or TYPE_PUBLIC
]);
```

#### Change the bucket Type
``` php
$updatedBucket = $client->updateBucket([
    'BucketId' => $bucket->getId(),
    'BucketType' => Bucket::TYPE_PUBLIC
]);
```

#### List all buckets
``` php
$buckets = $client->listBuckets([
    //Optional
    //'BucketName'=>string,
    //'BucketId'  =>string,
    //'BucketTypes'=>array
]);
```
#### Delete a bucket
``` php
$client->deleteBucket([
    'BucketId' => 'YOUR_BUCKET_ID' (or BucketName)
]);
```

#### File Upload
``` php
$file = $client->upload([
    'FileName' => 'path/to/upload/to',
    'BucketId' => 'string'
    'Body' => 'I am the file content',

    // The file content can also be provided via a resource.
    // 'Body' => fopen('/path/to/input', 'r')
    // Optional
    //'BucketName' => 'my-special-bucket',
    //FileContentType => 'The MIME type of the content of the file'
]);
```

#### File Download
``` php
$fileContent = $client->download([
    'FileId' => $file->getId() //Required only for download by ID

    // Can also identify the file via bucket and path:
    // 'BucketName' => 'my-special-bucket',
    // 'FileName' => 'path/to/file'

]);
```

#### File Delete
``` php
$client->deleteFile([
    'FileId' => $file->getId()

    // Can also identify the file via path:
    // 'FileName' => 'path/to/file'
]);
```

#### List all files
``` php
$fileList = $client->listFiles([
    'BucketId' => 'YOUR_BUCKET_ID'

    // Optional
    // 'BucketName'    =>'string',
    // 'StartFileName' =>'string',
    // 'MaxFileCount'  =>'string',
    // 'Prefix'        =>'string',
    // 'Delimiter'     =>'string',
]);
```

#### File get info
``` php
$file = $client->getFile(string $fileId);
```

#### Keys list
``` php
$keys = $client->listKeys([
    // Optional
    // 'MaxKeyCount'=>100,
    // 'StartApplicationKeyId'=>'keyId'
]);
```

#### Create a key
``` php
$key = $client->createKey([
    'Capabilities'=>array,
    'KeyName'=>string
    // Optional
    // 'BucketId'=>string,
    // 'NamePrefix'=>'path',
    // 'ValidDurationInSeconds'=>seconds
]);
```

#### Delete a key
``` php
$client->deleteKey(string $keyId);
```

#### Large file manage. Lists information about large file uploads that have been started, but have not been finished or canceled.
``` php
$client->listUnfinishedLargeFiles([
    'BucketId'=>'string'
    //optional
    // 'BucketName',
    // 'NamePrefix',
    // 'StartFileId'
    // 'MaxFileCount'
]);
```

#### Large file manage. Cancel the upload of a large file, and delete all of the parts that have been uploaded.
``` php
$client->cancelUnfinishedLargeFile(string $fileId);
```

## Security

If you discover any security related issues, please email misterspelik@gmail.com instead of using the issue tracker.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
