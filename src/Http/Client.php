<?php

namespace Backblaze\Http;

use Backblaze\Config\Config;
use Backblaze\Storage\Bucket;
use Backblaze\Storage\File;
use Backblaze\Storage\Key;
use Backblaze\Storage\FilePart;

use GuzzleHttp\Client as GuzzleClient;

use GuzzleHttp\Exception\GuzzleException;
use Backblaze\Exceptions\ErrorHandler;
use Backblaze\Exceptions\File\ErrorReadingLocalFileException;
use Backblaze\Exceptions\Config\ConfigWasntSetupException;
use Backblaze\Exceptions\ErrorResponseException;

use Backblaze\Traits\FileValidation;
use Backblaze\Traits\BucketValidation;

 /**
  * Client wrapper around Guzzle.
  *
  *
  * @property   string   $apiUrl
  * @property   string   $apiAuthToken
  * @property   string   $uploadUrl
  * @property   string   $uploadAuthToken
  * @property   string   $downloadAuthToken
  * @property   Backblaze\Config\Config     $config
  *
  * @method     bool                        isAuthorized()
  * @method     bool                        authorize()
  * @method     void                        authorizeDownload(string $bucketId, string $fileNamePrefix)
  * @method     array                       authorizedApiRequest($method, $uri, $json)
  * @method     array                       authorizedDownloadRequest($uri)
  * @method     mix                         request(string $method, $uri = null, array $options = [], $asJson = true)
  * @method     array                       getBucketsList(array $options)
  * @method     Backblaze\Storage\Bucket    createBucket(array $options)
  * @method     Backblaze\Storage\Bucket    updateBucket(array $options)
  * @method     Backblaze\Storage\Bucket    deleteBucket(array $options)
  * @method     Backblaze\Storage\File      upload(array $options)
  * @method     void                        getUploadUrl(string $bucketId)
  * @method     mix                         download(array $options)
  * @method     bool                        deleteFile(array $options)
  * @method     array                       getListFiles(array $options)
  * @method     Backblaze\Storage\File      getFile(string $fileId)
  * @method     Backblaze\Storage\File      uploadLargeFile(array $options)
  * @method     bool                        cancelUnfinishedLargeFile(string $fileId)
  * @method     array                       getListUnfinishedLargeFiles(array $options)
  * @method     array                       getPartsOfLargeFile(array $options)
  * @method     array                       startLargeFile(string $fileName, string $contentType, string $bucketId)
  * @method     array                       getUploadPartUrl(string $fileId)
  * @method     array                       uploadParts(string $filePath, string $uploadUrl, string $largeFileAuthToken)
  * @method     bool                        finishLargeFile(string $fileId, array $sha1s)
  * @method     array                       getKeysList(array $options = [])
  * @method     Backblaze\Storage\Key       createKey(array $options)
  * @method     bool                        deleteKey(string $applicationKeyId)
  * @method     string                      getFileNamePrefix(string $path)
  *
  */
class Client extends GuzzleClient
{
    use FileValidation, BucketValidation;

    private $apiUrl = null;
    private $apiAuthToken = null;

    private $uploadUrl = null;
    private $uploadAuthToken = null;
    private $downloadAuthToken = null;
    private $recommendedUploadPartSize = 100000000; //bytes

    /**
     * The Backblaze Config instance.
     *
     * @var Config
     */
    private $_config;

    public function __construct($options = [])
    {
        parent::__construct($options);

        $this->_config = Config::getInstance();
        if (!$this->_config->isSetUp()){
            throw new ConfigWasntSetupException();
        }
    }

    /**
    * @return bool
    **/
    public function isAuthorized(): bool
    {
        return (!is_null($this->apiAuthToken) && !is_null($this->apiUrl));
    }

    /**
    * Set properties apiAuthToken, apiUrl and downloadUrl
    *
    * @return bool
    **/
    public function authorize(): bool
    {
        if (!$this->_config->needReauth()){
            return true;
        }

        $credentials = base64_encode($this->_config->getApplicationKeyId() . ":" . $this->_config->getApplicationKey());

        $response = $this->request('GET', $this->_config->getApiAuthUrl(),[
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Basic ' . $credentials,
            ],
        ]);

        $this->apiAuthToken = $response['authorizationToken'];
        $this->apiUrl = $response['apiUrl'].'/'.$this->_config->getApiVersion();
        $this->downloadUrl = $response['downloadUrl'];
        $this->recommendedUploadPartSize = $response['recommendedPartSize'];

        $this->_config->touchReauthTime();

        return true;
    }

    /**
    * Set property downloadAuthToken
    *
    * @param string $bucketId
    * @param string $fileNamePrefix (If you have a private bucket named "photos"
    *                               and generate a download authorization token for the fileNamePrefix "pets/"
    *                               you will be able to use the download authorization token to access:
    *                               https://f345.backblazeb2.com/file/photos/pets/kitten.jpg)
    *
    * @return void
    **/
    public function authorizeDownload($bucketId, $fileNamePrefix) : void
    {
        $response = $this->authorizedApiRequest('POST', '/b2_get_download_authorization', [
            'bucketId'      => $bucketId,
            'fileNamePrefix'=> $fileNamePrefix,
            'validDurationInSeconds'=>$this->_config->getDownloadValidDurationTimeout()
        ]);

        $this->downloadAuthToken = $response['authorizationToken'];
    }

    /**
    * @param string $method
    * @param string $uri
    * @param array $json
    * @param array $headers
    *
    * @return array
    **/
    public function authorizedApiRequest($method, $uri, $json = [], $headers = [])
    {
        $this->authorize();

        return $this->request($method, $this->apiUrl.$uri, [
            'headers' => array_merge(['Authorization' => $this->apiAuthToken], $headers),
            'json' => $json,
        ]);
    }

    /**
    * @param string $uri
    *
    * @return array
    **/
    public function authorizedDownloadRequest($uri)
    {
        $this->authorize();

        return $this->request('GET', $this->downloadUrl.$uri, [
            'headers' => [
                'Authorization' => $this->downloadAuthToken ? $this->downloadAuthToken : $this->apiAuthToken
            ]
        ], false);
    }

    /**
     * Sends a response to the B2 API, automatically handling decoding JSON and errors.
     *
     * @param string $method
     * @param null   $uri
     * @param array  $options
     * @param bool   $asJson
     *
     * @throws GuzzleException
     *
     * @return mixed|Psr\Http\Message\ResponseInterface|string
     */
    public function request($method, $uri = null, array $options = [], $asJson = true)
    {
        $response = parent::request($method, $uri, $options);
        $decoded_response = json_decode($response->getBody(), true);

        if ($response->getStatusCode() !== 200) {
           ErrorHandler::handleErrorResponse($decoded_response);
        }

        if ($asJson) {
            return $decoded_response;
        }

        return $response->getBody()->getContents();
    }

    /**
    * Lists buckets associated with an account, in alphabetical order by bucket name.
    *
    * @param array $options
    *
    * @throws Backblaze\Exceptions\ErrorResponseException
    *
    * @return array
    **/
    public function getBucketsList(array $options = []): array
    {
        $response = $this->authorizedApiRequest('POST', '/b2_list_buckets', [
            'accountId'     => $this->_config->getApplicationKeyId(),
            'bucketName'    => $options['BucketName'] ?? null,
            'bucketId'      => $options['BucketId'] ?? null,
            'bucketTypes'   => [$options['BucketTypes'] ?? "all"]
        ]);

        $buckets = [];
        foreach ($response['buckets'] as $bucket) {
            $buckets[] = new Bucket($bucket['bucketId'], $bucket['bucketName'], $bucket['bucketType']);
        }

        return $buckets;
    }

    /**
     * Create a bucket with the given name and type.
     *
     * @param array $options
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return Backblaze\Storage\Bucket
     */
    public function createBucket(array $options) : Bucket
    {
        $response = $this->authorizedApiRequest('POST', '/b2_create_bucket', [
            'accountId'  => $this->_config->getApplicationKeyId(),
            'bucketName' => $options['BucketName'],
            'bucketType' => $options['BucketType'],
        ]);

        return new Bucket($response['bucketId'], $response['bucketName'], $response['bucketType']);
    }

    /**
     * Update a bucket with the given type.
     *
     * @param array $options
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return Backblaze\Storage\Bucket
     */
    public function updateBucket(array $options) : Bucket
    {
        $response = $this->authorizedApiRequest('POST', '/b2_update_bucket', [
            'accountId'  => $this->_config->getApplicationKeyId(),
            'bucketId'   => $options['BucketId'],
            'bucketType' => $options['BucketType'],
        ]);

        return new Bucket($response['bucketId'], $response['bucketName'], $response['bucketType']);
    }

    /**
     * Deletes the bucket identified by its ID.
     *
     * @param array $options
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     * @return bool
     */
    public function deleteBucket(array $options) : bool
    {
        $response = $this->authorizedApiRequest('POST', '/b2_delete_bucket', [
            'accountId' => $this->_config->getApplicationKeyId(),
            'bucketId'  => $options['BucketId'],
        ]);

        return true;
    }

    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\ErrorResponseException
    *
    * @return Backblaze\Storage\File
    **/
    public function upload(array $options) : File
    {
        $this->getUploadUrl($options['BucketId']);

        try {
            $response = $this->request('POST', $this->uploadUrl, [
                'headers' => [
                    'Authorization'                      => $this->uploadAuthToken,
                    'Content-Type'                       => $options['FileContentType'],
                    'Content-Length'                     => $options['size'],
                    'X-Bz-File-Name'                     => $options['FileName'],
                    'X-Bz-Content-Sha1'                  => $options['hash'],
                    'X-Bz-Info-src_last_modified_millis' => $options['FileLastModified'],
                ],
                'body' => $options['Body'],
            ]);

            return new File(
                $response['fileId'],
                $response['fileName'],
                $response['contentSha1'],
                $response['contentLength'],
                $response['contentType'],
                $response['fileInfo'],
                $response['bucketId'],
                $response['action'],
                $response['uploadTimestamp']
            );
        } catch (\Exception $e) {
            throw new ErrorResponseException($e->getMessage());
        }
    }

    /**
    * Set properties uploadUrl and uploadAuthToken
    *
    * @param string $bucketId
    *
    * @return void
    **/
    public function getUploadUrl($bucketId) : void
    {
        $response = $this->authorizedApiRequest('POST', '/b2_get_upload_url', [
            'bucketId' => $bucketId,
        ]);

        $this->uploadUrl = $response['uploadUrl'];
        $this->uploadAuthToken = $response['authorizationToken'];

    }

    /**
    * @param array $options
    *
    * @return array
    **/
    public function download(array $options)
    {
        $this->authorize();

        if (isset($options['FileId'])) {
            $requestUrl = sprintf('/%s/b2_download_file_by_id?fileId=%s', $this->_config->getApiVersion(), $options['FileId']);
        } else {
            $fileNamePrefix = $this->getFileNamePrefix($options['FileName']);
            $this->authorizeDownload($options['BucketId'], $fileNamePrefix);
            $requestUrl = sprintf('/file/%s/%s', $options['BucketName'], $options['FileName']);
        }

        return $this->authorizedDownloadRequest($requestUrl);
    }

    /**
     * Delete the file identified by ID from Backblaze B2.
     *
     * @param array $options
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    public function deleteFile(array $options) : bool
    {
        $response = $this->authorizedApiRequest('POST', '/b2_delete_file_version', [
            'fileId'   => $options['FileId'],
            'fileName' => $options['FileName'],
        ]);

        return true;
    }

    /**
     * Retrieve a collection of File objects representing the files stored inside a bucket.
     *
     * @param array $options
     *
     * @return array
     */
    public function getListFiles(array $options)
    {
        $files = [];
        $nextFileName = $options['StartFileName'] ?? null;
        do {
            $response = $this->authorizedApiRequest('POST', '/b2_list_file_names', [
                'bucketId'      => $options['BucketId'],
                'startFileName' => $nextFileName,
                'maxFileCount'  => $options['MaxFileCount'] ?? null,
                'prefix'        => $options['Prefix'] ?? null,
                'delimiter'     => $options['Delimiter'] ?? null,
            ]);

            foreach ($response['files'] as $file) {

                if (!$file['fileId']) {
                    continue;
                }

                $files[] = new File(
                    $file['fileId'],
                    $file['fileName'],
                    $file['contentSha1'],
                    $file['contentLength'],
                    $file['contentType'],
                    $file['fileInfo'],
                    $file['bucketId'],
                    $file['action'],
                    $file['uploadTimestamp']
                );
            }
            $nextFileName = $response['nextFileName'];
        } while ($nextFileName);

        return $files;
    }

    /**
     * Gets information about one file stored in B2.
     *
     * @param string $fileId
     *
     * @return Backblaze\Storage\File
     */
    public function getFile(string $fileId) : File
    {
        $response = $this->authorizedApiRequest('POST', '/b2_get_file_info', [
            'fileId' => $fileId
        ]);

        $file = new File(
            $response['fileId'],
            $response['fileName'],
            $response['contentSha1'],
            $response['contentLength'],
            $response['contentType'],
            $response['fileInfo'],
            $response['bucketId'],
            $response['action'],
            $response['uploadTimestamp']
        );

        return $file;
    }

    /**
     * Uploads a large file using b2 large file procedure.
     *
     * @param array $options
     *
     * @return Backblaze\Storage\File
     */
    public function uploadLargeFile(array $options) : File
    {

        // 1) b2_start_large_file, (returns fileId)
        $start = $this->startLargeFile($options['FileName'], $options['FileContentType'], $options['BucketId']);

        // 2) b2_get_upload_part_url for each thread uploading (takes fileId)
        $url = $this->getUploadPartUrl($start['fileId']);

        // 3) b2_upload_part for each part of the file
        $parts = $this->uploadParts($options['FilePath'].$options['FileName'], $url['uploadUrl'], $url['authorizationToken']);

        // $sha1s = [];
        // foreach ($parts as $part) {
        //     $sha1s[] = $part['contentSha1'];
        // }
        //
        // // 4) b2_finish_large_file.
        // return $this->finishLargeFile($start['fileId'], $sha1s);
        return new File(
            $start['fileId'],
            $start['fileName'],
            $start['contentSha1'],
            $start['contentLength'],
            $start['contentType'],
            $start['fileInfo'],
            $start['bucketId'],
            $start['action'],
            $start['uploadTimestamp']
        );
    }


    /**
     * Cancel the upload of a large file, and delete all of the parts that have been uploaded.
     *
     * @param string $fileId (The ID returned by b2_start_large_file)
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    public function cancelUnfinishedLargeFile(string $fileId) : bool
    {
        $this->authorizedApiRequest('POST', '/b2_cancel_large_file', [
            'fileId' => $fileId,
        ]);

        return true;
    }

    /**
     * Lists information about large file uploads that have been started,
     * but have not been finished or canceled.
     *
     * @param array $options
     *
     * @return array
     */
    public function getListUnfinishedLargeFiles(array $options) : array
    {
        $files = [];
        $nextFileId =  $options['StartFileId'] ?? null;
        do {
            $response = $this->authorizedApiRequest('POST', '/b2_list_unfinished_large_files', [
                'bucketId'      =>  $options['BucketId'],
                'namePrefix'    =>  $options['NamePrefix'] ?? null,
                'startFileId'   =>  $nextFileId,
                'namePrefix'    =>  $options['MaxFileCount'] ?? null,
            ]);

            foreach ($response['files'] as $file) {

                if (!$file['fileId']) {
                    continue;
                }

                $files[] = new File(
                    $file['fileId'],
                    $file['fileName'],
                    $file['contentSha1'],
                    $file['contentLength'],
                    $file['contentType'],
                    $file['fileInfo'],
                    $file['bucketId'],
                    $file['action'],
                    $file['uploadTimestamp']
                );
            }
            $nextFileId = $response['nextFileId'];
        } while ($nextFileId);

        return $files;
    }

    /**
     * Lists the parts that have been uploaded for a large file that has not been finished yet.
     *
     * @param array $options
     *
     * @return array
     */
    public function getPartsOfLargeFile(array $options)
    {
        $file_parts = [];
        $nextPartNumber = $options['StartPartNumber'] ?? null;
        do {
            $response = $this->authorizedApiRequest('POST', '/b2_list_parts', [
                'fileId'            =>  $options['FileId'],
                'startPartNumber'   =>  $nextPartNumber,
                'maxPartCount'      =>  $options['MaxPartCount'] ?? null,
            ]);

            foreach ($response['parts'] as $key => $part) {
                $file_parts[]   = new FilePart(
                    $part['fileId'],
                    $part['partNumber'],
                    $part['contentLength'],
                    $part['contentSha1'],
                    $part['contentMd5'],
                    $part['uploadTimestamp']
                );
            }
            $nextPartNumber = $response['nextPartNumber'];
        } while ($nextPartNumber);

        return $file_parts;
    }

    /**
     * Starts the large file upload process.
     *
     * @param string $fileName
     * @param string $contentType
     * @param string $bucketId
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return array
     */
    protected function startLargeFile(string $fileName, string $contentType, string $bucketId)
    {
        $response = $this->authorizedApiRequest('POST', '/b2_start_large_file', [
            'fileName'      => $fileName,
            'contentType'   => $contentType,
            'bucketId'      => $bucketId,
        ]);

        return $response;
    }

    /**
     * Gets the url for the next large file part upload.
     *
     * @param string $fileId
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return array
     */
    protected function getUploadPartUrl(string $fileId)
    {
        $response = $this->authorizedApiRequest('POST', '/b2_get_upload_part_url', [
            'fileId' => $fileId,
        ]);

        return $response;
    }

    /**
     * Uploads the file as "parts" of 100MB each (recommendedUploadPartSize).
     *
     * @param string $filePath
     * @param string $uploadUrl
     * @param string $largeFileAuthToken
     *
     * @throws Backblaze\Exceptions\ErrorReadingLocalFileException
     * @return array
     */
    protected function uploadParts(string $filePath, string $uploadUrl, string $largeFileAuthToken)
    {
        $parts = [];
        $bytes_sent_for_part = $this->recommendedUploadPartSize;
        $total_bytes_sent = 0;
        $sha1_of_parts = [];
        $part_no = 1;

        try {
            $local_file_size = filesize($filePath);
            $file_handle = fopen($filePath, 'r');
        } catch (\Exception $e) {
            throw new ErrorReadingLocalFileException($e->getMessage());
        }

        while ($total_bytes_sent < $local_file_size) {
            // Determine the number of bytes to send based on the minimum part size
            if (($local_file_size - $total_bytes_sent) < $this->recommendedUploadPartSize) {
                $bytes_sent_for_part = ($local_file_size - $total_bytes_sent);
            }

            // Get a sha1 of the part we are going to send
            fseek($file_handle, $total_bytes_sent);
            $data_part = fread($file_handle, $bytes_sent_for_part);
            array_push($sha1_of_parts, sha1($data_part));
            fseek($file_handle, $total_bytes_sent);

            // $request = new GuzzleHttp\Psr7\Request('POST', $uploadUrl, [
            //         'Authorization'    => $largeFileAuthToken,
            //         'Content-Length'   => $bytes_sent_for_part,
            //         'X-Bz-Part-Number' => $part_no,
            //         'X-Bz-Content-Sha1'=> $sha1_of_parts[$part_no - 1],
            //     ],
            //     $data_part
            // );
            // $this->sendAsync($request);
            $response = $this->request('POST', $uploadUrl, [
                'headers' => [
                    'Authorization'    => $largeFileAuthToken,
                    'Content-Length'   => $bytes_sent_for_part,
                    'X-Bz-Part-Number' => $part_no,
                    'X-Bz-Content-Sha1'=> $sha1_of_parts[$part_no - 1],
                ],
                'body' => $data_part,
            ]);
            //TODO: fix the problem out of memory limit
            // cause of problem Gazzle HTTP request
            $parts[] = $response;

            // Prepare for the next iteration of the loop
            $part_no++;
            $total_bytes_sent = $bytes_sent_for_part + $total_bytes_sent;
        }

        fclose($file_handle);

        return $parts;
    }

    /**
     * Finishes the large file upload procedure.
     *
     * @param string $fileId
     * @param array $sha1s
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    protected function finishLargeFile(string $fileId, array $sha1s) : bool
    {
        $response = $this->authorizedApiRequest('POST', '/b2_finish_large_file', [
                'fileId'        => $fileId,
                'partSha1Array' => $sha1s,
            ],
            [
                'synchronous'=>false
            ]
        );

        return true;
    }


    /**
     * Lists application keys associated with an account.
     *
     * @param array $options
     * MaxKeyCount           (Default is 100, maximum is 10000)
     * StartApplicationKeyId (Used when a query hits the maxKeyCount,
     *                       and you want to get more. Set to the value returned as the
     *                       nextApplicationKeyId in the previous query.)
     * ]
     *
     * @return array
     */
    public function getKeysList(array $options = [])
    {
        $keys = [];
        $nextApplicationKeyId = $options['StartApplicationKeyId'] ?? null;
        do {
            $response = $this->authorizedApiRequest('POST', '/b2_list_keys', [
                'accountId'     => $this->_config->getApplicationKeyId(),
                'maxKeyCount'   => empty($options['MaxKeyCount']) ? 100 : $options['MaxKeyCount'],
                'startApplicationKeyId'=>$nextApplicationKeyId
            ]);

            foreach ($response['keys'] as $key) {
                $keys[] = new Key(
                    $key['applicationKeyId'],
                    $key['keyName'],
                    $key['capabilities'],
                    $key['accountId'],
                    $key['expirationTimestamp'],
                    $key['bucketId'],
                    $key['namePrefix']
                );
            }
            $nextApplicationKeyId = $response['nextApplicationKeyId'];
        } while ($nextApplicationKeyId);

        return $keys;
    }


    /**
     * Creates a new application key.
     *
     * @param array $options
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return Backblaze\Storage\Key
     */
    public function createKey(array $options) : Key
    {
        $key = $this->authorizedApiRequest('POST', '/b2_create_key', [
            'accountId'             =>  $this->_config->getApplicationKeyId(),
            'capabilities'          =>  $options['Capabilities'],
            'keyName'               =>  $options['KeyName'],
            'validDurationInSeconds'=>  $options['ValidDurationInSeconds'] ?? null,
            'bucketId'              =>  $options['BucketId'] ?? null,
            'namePrefix'            =>  $options['NamePrefix'] ?? null,
        ]);

        return new Key(
            $key['applicationKeyId'],
            $key['keyName'],
            $key['capabilities'],
            $key['accountId'],
            $key['expirationTimestamp'],
            $key['bucketId'],
            $key['namePrefix']
        );
    }

    /**
     * Deletes the application key specified.
     *
     * @param string $applicationKeyId
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    public function deleteKey(string $applicationKeyId) : bool
    {
        $key = $this->authorizedApiRequest('POST', '/b2_delete_key', [
            'applicationKeyId' =>  $applicationKeyId,
        ]);

        return true;
    }


    /**
     * Get file path. E.g. /file/photos/cute/kitten.jpg return cute
     *
     * @param string $path
     *
     * @return bool
     */
    private function getFileNamePrefix(string $path) : string
    {
        $segments = explode('/', $path);
        $fileName = array_pop($segments);

        return implode('/', $segments);
    }
}
