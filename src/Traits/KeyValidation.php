<?php

namespace Backblaze\Traits;

use Backblaze\Storage\Key;
use Backblaze\Exceptions\InvalidRequestParamException;
use Backblaze\Exceptions\ErrorResponseException;

trait KeyValidation
{
    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateCreateKeyRequest(array $options) : void
    {
        if (empty($options['Capabilities']) || empty($options['KeyName'])) {
            throw new InvalidRequestParamException('The list of Capabilities and KeyName are required.');
        }
    }
}
