<?php

namespace Backblaze\Traits;

use Backblaze\Storage\Bucket;
use Backblaze\Exceptions\InvalidRequestParamException;

trait BucketValidation
{
    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateBucketType(array $options) : void
    {
        if (empty($options['BucketType']) || !in_array($options['BucketType'], [Bucket::TYPE_PUBLIC, Bucket::TYPE_PRIVATE])) {
            throw new InvalidRequestParamException("Bucket type must be allPublic, allPrivate or snapshot");
        }
    }
}
