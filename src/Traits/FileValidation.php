<?php

namespace Backblaze\Traits;

use Backblaze\Exceptions\File\ToLargeFileException;
use Backblaze\Exceptions\InvalidRequestParamException;

trait FileValidation
{
    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateBucketIdParameter(array $options)
    {
        if (empty($options['BucketName']) && empty($options['BucketId'])) {
            throw new InvalidRequestParamException('The BucketName or BucketId are required.');
        }
    }

    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateDownloadParams(array $options)
    {
        if (!empty($options['FileName']) && empty($options['BucketName']) && empty($options['BucketId'])) {
            throw new InvalidRequestParamException('The BucketName or BucketId are required.');
        }

        if ((empty($options['FileId']) && empty($options['FileName']))) {
            throw new InvalidRequestParamException('The FileName or FileId are required.');
        }
    }

    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateDeleteFileRequestParameters(array $options)
    {
        if ((empty($options['FileId']) && empty($options['FileName']))) {
            throw new InvalidRequestParamException('The FileName or FileId are required.');
        }
    }

    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateFilePartsRequestParameters(array $options)
    {
        if ((empty($options['FileId']))) {
            throw new InvalidRequestParamException('The FileId is required.');
        }
    }

    /**
    * @param array $options
    *
    * @throws Backblaze\Exceptions\InvalidRequestParamException
    **/
    public function validateUploadLargeFileRequestParams(array $options)
    {
        if (empty($options['BucketName']) && empty($options['BucketId'])) {
            throw new InvalidRequestParamException('The BucketName or BucketId are required.');
        }

        if (empty($options['FileName'])) {
            throw new InvalidRequestParamException('The FileName is required.');
        }

        if (empty($options['FilePath'])) {
            throw new InvalidRequestParamException('The FilePath is required.');
        }
    }

    /**
    * The file size can't be more than 100Mb
    *
    * @param integer $fileSize (bytes)
    *
    * @throws Backblaze\Exceptions\File\ToLargeFileException
    **/
    public function validateFileSize($fileSize)
    {
        // Bytes to Mb
        if ($fileSize/1000000 > 100) {
            throw new ToLargeFileException();
        }
    }
}
