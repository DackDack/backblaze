<?php

namespace Backblaze;

use Backblaze\Storage\File;
use Backblaze\Storage\Bucket;
use Backblaze\Storage\Key;
use Backblaze\Traits\BucketValidation;
use Backblaze\Traits\FileValidation;
use Backblaze\Traits\KeyValidation;
use Backblaze\Config\Config;
use Backblaze\Http\Client as HttpClient;

class Client
{
    use BucketValidation,
        FileValidation,
        KeyValidation;

    /**
     * The Config instance.
     *
     * @var Config
     */
    private $config;

    /**
     * The http client.
     *
     * @var Backblaze\Http\Client
     */
    private $http;

    /**
    * Application keys are used as a pair: Key ID and Application Key.
    *
    * @param string $applicationKeyId
    * @param string $applicationKey
    * @param array $options [
    *       auth_timeout_seconds
    *       download_valid_duration_seconds
    * ]
    **/
    public function __construct($applicationKeyId, $applicationKey, $options = [])
    {
        $this->config = Config::getInstance();
        $this->config->setUp($applicationKeyId, $applicationKey, $options);

        $this->http = new HttpClient(['exceptions' => false]);
    }

    /**
     * Uploads a file to a bucket and returns a File object.
     * https://www.backblaze.com/b2/docs/b2_upload_file.html
     *
     * @param array $options [
     *      FileName        required
     *      BucketId        required
     *      Body            required    (resource)
     *      BucketName      optional
     *      FileContentType optional    (The MIME type of the content of the file)
     * ]
     *
     * @throws Backblaze\Exceptions\File\ToLargeFileException
     *
     * @return Backblaze\Storage\File
     */
    public function upload(array $options) : File
    {
        $options['FileName'] = File::normalizeName($options['FileName']);

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        if (is_resource($options['Body'])) {
            // We need to calculate the file's hash incrementally from the stream.
            $context = hash_init('sha1');
            hash_update_stream($context, $options['Body']);
            $options['hash'] = hash_final($context);

            // Similarly, we have to use fstat to get the size of the stream.
            $options['size'] = fstat($options['Body'])['size'];

            // Rewind the stream before passing it to the HTTP client.
            rewind($options['Body']);
        } else {
            // We've been given a simple string body, it's super simple to calculate the hash and size.
            $options['hash'] = sha1($options['Body']);
            $options['size']= strlen($options['Body']);
        }

        if (!isset($options['FileLastModified'])) {
            $options['FileLastModified'] = round(microtime(true) * 1000);
        }

        if (!isset($options['FileContentType'])) {
            $options['FileContentType'] = 'b2/x-auto';
        }

        return $this->http->upload($options);
    }

    /**
     * Uploads a large file using b2 large file procedure.
     * https://www.backblaze.com/b2/docs/large_files.html
     *
     * @param array $options [
     *      FileName    required (e.g. test.zip)
     *      FilePath    required (e.g. /home/download/)
     *      BucketId    required
     *      BucketName  optional
     *      FileContentType optional (e.g. application/zip)
     * ]
     *
     * @throws Backblaze\Exceptions\ErrorResponseException|InvalidRequestParamException
     *
     * @return Backblaze\Storage\File
     */
    public function uploadLargeFile(array $options) : File
    {
        $this->validateUploadLargeFileRequestParams($options);

        if (substr($options['FileName'], 0, 1) === '/') {
            $options['FileName'] = ltrim($options['FileName'], '/');
        }

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        //if last char of path is not a "/" then add a "/"
        if (substr($options['FilePath'], -1) != '/') {
            $options['FilePath'] = $options['FilePath'].'/';
        }

        if (!isset($options['FileContentType'])) {
            $options['FileContentType'] = 'b2/x-auto';
        }

        return $this->http->uploadLargeFile($options);
    }

    /**
     * Download file.
     * https://www.backblaze.com/b2/docs/b2_download_file_by_id.html
     * https://www.backblaze.com/b2/docs/b2_download_file_by_name.html
     *
     * @param array $options [
     *      FileId (Required for download by ID)
     *      FileName and BucketId or BucketName (Required for download by Name)
     * ]
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return mix
     */
    public function download(array $options)
    {
        $this->validateDownloadParams($options);

        if (!isset($options['BucketName']) && isset($options['BucketId'])) {
            $options['BucketName'] = $this->getBucketNameFromId($options['BucketId']);
        }

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        return $this->http->download($options);
    }


    /**
     * Returns a list of bucket objects representing the buckets on the account.
     * https://www.backblaze.com/b2/docs/b2_list_buckets.html
     *
     * @param array $options [
     *      BucketName  optional  (When bucketName is specified, the result will be a list containing just this bucket)
     *      BucketId    optional (When bucketId is specified, the result will be a list containing just this bucket,
     *                           if it's present in the account, or no buckets if the account does not have a bucket
     *                           with this ID.)
     *      BucketTypes optional (If present, B2 will use it as a filter for bucket types returned in the list buckets response.
     *                           "allPublic", "allPrivate", "snapshot" or "all")
     * ]
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return array
     */
    public function listBuckets(array $options = []) : array
    {
        return $this->http->getBucketsList($options);
    }

    /**
     * Create a bucket with the given name and type.
     * https://www.backblaze.com/b2/docs/b2_create_bucket.html
     *
     * @param array $options [
     *      BucketName required
     *      BucketType required
     * ]
     *
     * @throws Backblaze\Exceptions\InvalidRequestParamException
     *
     * @return Backblaze\Storage\Bucket
     */
    public function createBucket(array $options) : Bucket
    {
        $this->validateBucketType($options);

        return $this->http->createBucket($options);
    }

    /**
     * Update the bucket with the given type.
     * https://www.backblaze.com/b2/docs/b2_update_bucket.html
     *
     * @param array $options [
     *      BucketId    required
     *      BucketType  required ["allPublic", "allPrivate"]
     * ]
     * @throws Backblaze\Exceptions\InvalidRequestParamException
     *
     * @return Backblaze\Storage\Bucket
     */
    public function updateBucket(array $options) : Bucket
    {
        $this->validateBucketType($options);

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        return $this->http->updateBucket($options);
    }

    /**
     * Delete the bucket
     * https://www.backblaze.com/b2/docs/b2_delete_bucket.html
     *
     * @param array $options [
     *      BucketId required if BucketName not present
     *      BucketName required if BucketId not present
     * ]
     *
     * @throws Backblaze\Exceptions\Backet\ErrorBucketResponseException
     *
     * @return boolean
     */
    public function deleteBucket(array $options) : bool
    {
        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        return $this->http->deleteBucket($options);
    }

    /**
     * Retrieve a collection of File objects representing the files stored inside a bucket.
     * https://www.backblaze.com/b2/docs/b2_list_file_names.html
     *
     * @param array $options [
     *      BucketId        required
     *      BucketName      optional
     *      StartFileName   optional
     *      MaxFileCount    optional    (The default value is 100, and the maximum is 10000.
     *                                  Passing in 0 means to use the default of 100.)
     *      Prefix          optional    (Files returned will be limited to those with the given prefix.)
     *      Delimiter       optional    (Files returned will be limited to those within the top folder, or any one subfolder.)
     * ]
     *
     * @return array
     */
    public function listFiles(array $options) : array
    {
        $this->validateBucketIdParameter($options);

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        return $this->http->getListFiles($options);

    }

    /**
     * Gets information about one file stored in B2.
     * https://www.backblaze.com/b2/docs/b2_get_file_info.html
     *
     * @param string $fileId
     *
     * @return Backblaze\Storage\File
     */
    public function getFile(string $fileId) : File
    {
        return $this->http->getFile($fileId);

    }

    /**
     * Delete the file identified by ID from Backblaze B2.
     * https://www.backblaze.com/b2/docs/b2_delete_file_version.html
     *
     * @param array $options [
     *      FileId      required if FileName not present
     *      FileName    required if FileId not present
     * ]
     *
     * @throws Backblaze\Exceptions\ErrorResponseException|InvalidRequestParamException
     *
     * @return bool
     */
    public function deleteFile(array $options)
    {
        $this->validateDeleteFileRequestParameters($options);

        if (!isset($options['FileName']) && isset($options['FileId'])) {
            $file = $this->getFile($options['FileId']);
            $options['FileName'] = $file->getName();
        }

        return $this->http->deleteFile($options);
    }

    /**
     * Cancel the upload of a large file, and delete all of the parts that have been uploaded.
     * https://www.backblaze.com/b2/docs/b2_cancel_large_file.html
     *
     * @param string $fileId (The ID returned by b2_start_large_file)
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    public function cancelUnfinishedLargeFile(string $fileId) : bool
    {
        return $this->http->cancelUnfinishedLargeFile($fileId);
    }

    /**
     * Lists information about large file uploads that have been started,
     * but have not been finished or canceled.
     * https://www.backblaze.com/b2/docs/b2_list_unfinished_large_files.html
     *
     * @param array $options [
     *      BucketId required
     *      BucketName optional
     *      NamePrefix optional (When a namePrefix is provided, only files whose names match the prefix will be returned)
     *      StartFileId optional
     *      MaxFileCount optional (The default value is 100, and the maximum allowed is 100.)
     * ]
     * @return array
     */
    public function listUnfinishedLargeFiles(array $options) : array
    {
        $this->validateBucketIdParameter($options);

        if (!isset($options['BucketId']) && isset($options['BucketName'])) {
            $options['BucketId'] = $this->getBucketIdFromName($options['BucketName']);
        }

        return $this->http->getListUnfinishedLargeFiles($options);
    }

    /**
     * Lists the parts that have been uploaded for a large file that has not been finished yet.
     * https://www.backblaze.com/b2/docs/b2_list_parts.html
     *
     * @param array $options [
     *      FileId          required
     *      StartPartNumber optional
     *      MaxPartCount    optional (The default value is 100, and the maximum allowed is 1000)
     * ]
     * @return array
     */
    public function listPartsOfLargeFile(array $options) : array
    {
        $this->validateFilePartsRequestParameters($options);

        return $this->http->getPartsOfLargeFile($options);
    }

    /**
     * Lists application keys associated with an account.
     * https://www.backblaze.com/b2/docs/b2_list_keys.html
     *
     * @param array $options [
     *      MaxKeyCount             optional    (Default is 100, maximum is 10000)
     *      StartApplicationKeyId   optional    (Used when a query hits the maxKeyCount,
     *                                          and you want to get more. Set to the value returned as the
     *                                          nextApplicationKeyId in the previous query.)
     * ]
     * @return array
     */
    public function listKeys(array $options = []) : array
    {
        return $this->http->getKeysList($options);
    }

    /**
     * Creates a new application key.
     * https://www.backblaze.com/b2/docs/b2_create_key.html
     *
     * @param array $options [
     *      Capabilities required   (Possibilities are: listKeys, writeKeys,
     *                              deleteKeys, listBuckets, writeBuckets,
     *                              deleteBuckets, listFiles, readFiles,
     *                              shareFiles, writeFiles, and deleteFiles)
     *      KeyName     required
     *      BucketId    optional    (When present, the new key can only access this bucket)
     *      NamePrefix  optional    (When present, restricts access to files whose names start with the prefix.
     *                              You must set bucketId when setting this.)
     *      ValidDurationInSeconds optional
     * ]
     *
     * @throws Backblaze\Exceptions\InvalidRequestParamException
     *
     * @return Backblaze\Storage\Key
     */
    public function createKey(array $options) : Key
    {
        $this->validateCreateKeyRequest($options);

        return $this->http->createKey($options);
    }

    /**
     * Deletes the application key specified.
     * https://www.backblaze.com/b2/docs/b2_delete_key.html
     *
     * @param string $applicationKeyId
     *
     * @throws Backblaze\Exceptions\ErrorResponseException
     *
     * @return boolean
     */
    public function deleteKey(string $applicationKeyId) : bool
    {
        return $this->http->deleteKey($applicationKeyId);
    }

    /**
    * @param string $bucketName
    *
    * @return mix
    **/
    protected function getBucketIdFromName(string $bucketName) : ?string
    {
        $buckets = $this->http->getBucketsList();
        foreach ($buckets as $bucket) {
            if ($bucket->getName() === $bucketName) {
                return $bucket->getId();
            }
        }
        return null;
    }

    /**
     * Maps the provided bucket ID to the appropriate bucket name.
     *
     * @param string $bucketId
     *
     * @return mixed
     */
    protected function getBucketNameFromId(string $bucketId) : ?string
    {
        $buckets = $this->http->getBucketsList();
        foreach ($buckets as $bucket) {
            if ($bucket->getId() === $bucketId) {
                return $bucket->getName();
            }
        }
        return null;
    }
}
