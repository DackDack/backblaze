<?php

namespace Backblaze\Exceptions;

use Throwable;

class ErrorHandler
{
    /**
     *
     * @throws ErrorResponseException
     */
    public static function handleErrorResponse($response)
    {
        if (!empty($response['status']) && $response['status'] != 200 && !empty($response['message'])) {
            throw new ErrorResponseException($response['message']);
        }
    }
}
