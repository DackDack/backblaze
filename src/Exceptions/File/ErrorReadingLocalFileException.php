<?php

namespace Backblaze\Exceptions\File;

use Backblaze\Exceptions\BaseException;

class ErrorReadingLocalFileException extends BaseException
{
}
