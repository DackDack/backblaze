<?php

namespace Backblaze\Exceptions\File;

use Backblaze\Exceptions\BaseException;

class ToLargeFileException extends BaseException
{
    protected $default_message = 'The file size is more than 100Mb. Use the uploadLargeFile() method to upload files bigest then 100Mb.';
}
