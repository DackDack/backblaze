<?php

namespace Backblaze\Exceptions;

use Backblaze\Exceptions\BaseException;

class InvalidRequestParamException extends BaseException
{
}
