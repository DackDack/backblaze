<?php

namespace Backblaze\Storage;

/**
 * FilePart class
 *
 *
 * @property   string   $fileId
 * @property   string   $partNumber
 * @property   string   $contentLength
 * @property   string   $contentSha1
 * @property   string   $contentMd5
 * @property   string   $uploadTimestamp
 *
 * @method     string   getFileId()
 * @method     string   getPartNumber()
 * @method     string   getContentLength()
 * @method     string   getHashSha1()
 * @method     string   getHashMd5()
 * @method     string   getUploadTimestamp()
 **/
class FilePart
{
    protected $fileId;
    protected $partNumber;
    protected $contentLength;
    protected $contentSha1;
    protected $contentMd5;
    protected $uploadTimestamp;

    /**
     * FilePart constructor.
     *
     * @param $fileId
     * @param $partNumber
     * @param $contentLength
     * @param $contentSha1
     * @param $contentMd5
     * @param $uploadTimestamp
     */
    public function __construct($fileId, $partNumber, $contentLength, $contentSha1, $contentMd5 = null, $uploadTimestamp = null)
    {
        $this->fileId           = $fileId;
        $this->partNumber       = $partNumber;
        $this->contentLength    = $contentLength;
        $this->contentSha1      = $contentSha1;
        $this->contentMd5       = $contentMd5;
        $this->uploadTimestamp  = $uploadTimestamp;
    }

    /**
     * @return string
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @return string
     */
    public function getPartNumber()
    {
        return $this->partNumber;
    }

    /**
     * @return string
     */
    public function getContentLength()
    {
        return $this->contentLength;
    }

    /**
     * @return string
     */
    public function getHashSha1()
    {
        return $this->contentSha1;
    }

    /**
     * @return string
     */
    public function getHashMd5()
    {
        return $this->contentMd5;
    }

    /**
     * @return string
     */
    public function getUploadTimestamp()
    {
        return $this->uploadTimestamp;
    }
}
