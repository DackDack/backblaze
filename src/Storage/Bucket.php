<?php

namespace Backblaze\Storage;

/**
 * Bucket class.
 *
 *
 * @property    string  $id
 * @property    string  $name
 * @property    string  $type

 *
 * @method     string   getId()
 * @method     string   getName()
 * @method     string   getType()
 **/
class Bucket
{
    /**
    * "allPublic" means that anybody can download the files is the bucket
    **/
    const TYPE_PUBLIC = 'allPublic';

    /**
    * "allPrivate" means that you need an authorization token to download them
    **/
    const TYPE_PRIVATE = 'allPrivate';

    /**
    * "snapshot" means that it's a private bucket containing snapshots created on the B2 web site
    **/
    const TYPE_SNAPSHOT = 'snapshot';

    protected $id;
    protected $name;
    protected $type;

    /**
     * Bucket constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $type
     */
    public function __construct(string $id, string $name, string $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }
}
