<?php

namespace Backblaze\Storage;

/**
 * Key class.
 *
 *
 * @property    string  $id
 * @property    string  $name
 * @property    array   $capabilities
 * @property    string  $accountId
 * @property    integer $expirationTimestamp
 * @property    string  $bucketId
 * @property    string  $namePrefix
 *
 * @method     string   getId()
 * @method     string   getName()
 * @method     array   getCapabilities()
 * @method     string   getAccountId()
 * @method     string   getExpirationTimestamp()
 * @method     string   getBucketId()
 * @method     string   getNamePrefix()
 **/
class Key
{
    const CAN_LIST_KEYS         = 'listKeys';
    const CAN_WRITE_KEYS        = 'writeKeys';
    const CAN_DELETE_KEYS       = 'deleteKeys';
    const CAN_LIST_BUCKETS      = 'listBuckets';
    const CAN_WRITE_BUCKETS     = 'writeBuckets';
    const CAN_DELETE_BUCKETS    = 'deleteBuckets';
    const CAN_LIST_FILES        = 'listFiles';
    const CAN_READ_FILES        = 'readFiles';
    const CAN_SHARE_FILES       = 'shareFiles';
    const CAN_WRITE_FILES       = 'writeFiles';
    const CAN_DELETE_FILES      = 'deleteFiles';

    protected $id;
    protected $name;
    protected $capabilities;
    protected $accountId;
    protected $expirationTimestamp;
    protected $bucketId;
    protected $namePrefix;

    /**
     * Key constructor.
     *
     * @param string $id
     * @param string $name
     * @param array $capabilities
     * @param string $accountId
     * @param integer $expirationTimestamp
     * @param string $bucketId
     * @param string $namePrefix
     */
    public function __construct(string $id, string $name, array $capabilities, string $accountId, $expirationTimestamp = null, $bucketId = null, $namePrefix = null)
    {
        $this->id           =   $id;
        $this->name         =   $name;
        $this->capabilities =   $capabilities;
        $this->accountId    =   $accountId;
        $this->expirationTimestamp  =   $expirationTimestamp;
        $this->bucketId     =   $bucketId;
        $this->namePrefix   =   $namePrefix;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCapabilities()
    {
        return $this->capabilities;
    }

    public function getAccountId()
    {
        return $this->accountId;
    }

    public function getExpirationTimestamp()
    {
        return $this->expirationTimestamp;
    }

    public function getBucketId()
    {
        return $this->bucketId;
    }

    public function getNamePrefix()
    {
        return $this->namePrefix;
    }
}
